/*global define, Backbone, _*/
'use strict';
define(['app'], function(App) {
  var AgaveSystem = {};

  AgaveSystem.List = Backbone.View.extend({
    template: 'systems/list',
    initialize: function() {
      this.collection.on('reset', this.render, this);
      this.collection.fetch({reset:true});
    },
    serialize: function() {
      return {systems: this.collection.toJSON()};
    },
    events: {
      'click .view-system': 'viewSystem'
    },
    viewSystem: function(e) {
      e.preventDefault();
      var models = this.collection.where({id: e.target.dataset.id});
      if (models) {
        var systemModel = new Backbone.Agave.Systems.System({id: models[0].id});
        var view = new AgaveSystem.SystemView({model: systemModel}); //models[0]});
        App.Layouts.main.setView('.content', view);
        view.render();
        App.router.navigate('#systems/' + models[0].id);
        $('html,body').animate({scrollTop:view.$el.position().top - 100});
      }
      return false;
    }
  });


  AgaveSystem.SystemView = Backbone.View.extend({
    template: 'systems/view',
    initialize: function() {
      this.model.on('change', this.render, this);
      if (! this.model.collection) {
        this.model.fetch();
      }
    },
    serialize: function() {
      var json = this.model.toJSON();
      if (this.model.collection) {
        json.hasCollection = true;
      }
      var systemType = json.type;
      json.isExecutionSystem = (json.type === "EXECUTION");
      json.isBrowsable = (! (json.public && json.isExecutionSystem));
      console.log(json);
      return json;
    },
    events: {
      'click .back-to-list': 'backToCollection',
      'click .btn-show-details': 'showDetails',
      'click .btn-show-roles': 'showRoles'
    },
    backToCollection: function(e) {
      e.preventDefault();
      this.remove();
      return false;
    },
    showDetails: function(e) {
      if (App.Agave.token().isValid()) {
        new AgaveSystem.SystemDetails({
          model: this.model,
          el: this.$el.find('.system-details')
        }).render();
        $(e.target).hide();
        this.$el.find('.btn-show-roles').show();
      } else {
        alert('You must be logged in to use systems.');
      }
    },
    showRoles: function(e) {
      if (App.Agave.token().isValid()) {
        new AgaveApps.SystemRoles({
          collection: new Backbone.Agave.Systems.SystemRoleList({ id: this.model.id }),
          el: this.$el.find('.system-details')
        }).render();
        $(e.target).hide();
        this.$el.find('.btn-show-details').show();
      } else {
        alert('You must be logged in to use systems.');
      }
    }
  });

  AgaveSystem.SystemDetails = Backbone.View.extend({
    tagName:'details',
    template: 'systems/details',
    initialize: function() {
      this.model.fetch();
    },
    serialize: function() {
      var json = this.model.toJSON();
      json.isExecutionSystem = (json.type === "EXECUTION");
      json.type = json.type.toLowerCase();
      console.log(this.model.toJSON());
      return json;
    }
  });


  AgaveSystem.ListItem = Backbone.View.extend({
    tagName: 'li',
    template: 'systems/list',
    serialize: function() {
      var json = this.model.toJSON();
      json.isExecutionSystem = (json.type === "EXECUTION");
      json.type = json.type.toLowerCase();
      console.log(this.model.toJSON());
      return json;
    }
  });

  AgaveSystem.CreateForm = Backbone.View.extend({
    tagName:'form',
    template: 'systems/form',
    initialize: function() {
      this.model = new Backbone.Agave.Systems.System();
    },
    beforeRender: function() {
      var FormViews = App.Views.FormViews;

      this.insertViews({'.form-fields': [
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Target URL',
            help: 'This is the URL that will be invoked by this System',
            name: 'url',
            id: 'url',
            defaultValue: this.model.get('url'),
            required: true
          })
        }),
        new FormViews.Select({
          model: new App.Models.FormModel({
            label: 'Target Method',
            help: 'This is the HTTP Method with which this URL will be invoked',
            name: 'method',
            id: 'method',
            defaultValue: this.model.get('method'),
            options: ['GET','POST','PUT','DELETE'],
            required: true
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Number of uses',
            help: 'How many times can this System be used?  For unlimited uses enter &quot;-1.&quot;',
            name: 'maxUses',
            id: 'maxUses',
            defaultValue: this.model.get('maxUses') || 1
          })
        }),
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Valid for (seconds)',
            help: 'How long should this System be valid?<br>30 days = 2592000<br>1 day = 604800<br>1 hour = 3600',
            name: 'lifetime',
            id: 'lifetime',
            defaultValue: this.model.get('lifetime') || 2592000
          })
        }),
        new FormViews.Checkbox({
          model: new App.Models.FormModel({
            label: 'Unauthenticated URL',
            help: 'If this url does not need authentication to be invoked, check this box',
            name: 'noauth',
            id: 'noauth',
            defaultValue: this.model.get('noauth')
          })
        }),
      ]});

      if (! App.Agave.token().isValid()) {
        this.insertView('.form-fields',
          new FormViews.Text({
            model: new App.Models.FormModel({
              label: 'Your iPlant Username',
              help: 'Provide your iPlant Username for pre-authenticating your System.',
              name: 'username',
              id: 'username',
              required: true
            })
          })
        );
      }

      this.insertViews({'.form-actions': [
        new FormViews.Button({
          'name': 'system-create-submit',
          'id': 'system-create-submit',
          'type': 'submit',
          'class': 'btn btn-success',
          'value': 'Create Post It'
        }),
        new FormViews.Button({
          'name': 'system-create-cancel',
          'id': 'system-create-cancel',
          'value': 'Cancel'
        })
      ]});
    },
    events: {
      'click #system-create-submit': 'submitForm',
      'click #system-create-cancel': 'cancelForm'
    },
    submitForm: function(e) {
      e.preventDefault();
      var formValues = this.$el.serializeArray();
      var post = {};

      _.each(formValues, function(value) {
        post[value.name] = value.value;
      });
      this.model.save({}, {
        emulateJSON: true,
        data: post,
        success: function() {
          Backbone.history.navigate('#systems', {'trigger':true});
        }
      });
      return false;
    },
    cancelForm: function(e) {
      e.preventDefault();
      var fragment;
      if (App.Agave.token().isValid()) {
        fragment = '#systems';
      } else {
        fragment = '#';
      }
      Backbone.history.navigate(fragment, {'trigger':true});
      return false;
    }
  });

  App.Views.AgaveSystem = AgaveSystem;
  return AgaveSystem;
});
