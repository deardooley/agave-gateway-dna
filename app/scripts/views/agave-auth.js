/*global define, Backbone, $, moment, alert*/
'use strict';
define(['app'], function(App) {
  var AgaveAuth = {}, UtilViews = App.Views.Util;

  AgaveAuth.NewTokenForm = Backbone.View.extend({
    template: 'auth/new-token-form',
    serialize: function() {
      return {
        //'username': this.model.get('username')
        'username': '',
        'client_key': '',
        'client_secret': '',
        'password': ''
      };
    },
    events: {
      'submit form': 'submitForm',
      'click .oauthd-submit': 'oauthdLogin'
    },
    oauthdLogin: function(e) {
      e.preventDefault();
      var that = this;
      OAuth.initialize('ItGn0eTyFGTaUgQg20Esb_KwTOY');

      OAuth.popup('agave', {state:'21344332abc'}, function(error, result) {
        //handle error with error
        if (error) {
          App.Agave.destroyToken();
          that.$el.find('.alert-info').html("Login failed!");
        } else {
          App.Agave.token({ 'access_token': result.access_token, 'expires_in': result.expires_in * 1000, 'expires_at': (new Date().getTime() + (result.expires_in * 1000)) } ).store();
          var profile = new Backbone.Agave.Profile.Profile({username:'me'});
          profile.fetch({
            success: function() {
              App.Agave.token({ 'username': profile.get('username'), 'access_token': result.access_token, 'expires_in': result.expires_in * 1000, 'expires_at': (new Date().getTime() + (result.expires_in * 1000)) } ).store();

              App.router.navigate('profile', {
                trigger: true
              });
            }
          });
        }
      });
    },
    submitForm: function(e) {
      e.preventDefault();
      var username = this.$el.find('#username').val(),
        password = this.$el.find('#password').val(),
        clientSecret = this.$el.find('#client_secret').val(),
        clientKey = this.$el.find('#client_key').val();
      if (username && password) {
        var message = new App.Models.MessageModel({
          'header': 'Getting token',
          'body': '<p>Please wait while we authenticate you...</p>'
        }),
          modal = new UtilViews.ModalMessage({
            model: message,
            backdrop: 'static',
            keyboard: false
          }),
          that = this;
        $('<div class="login-modal">').appendTo(this.el);
        modal.$el.on('shown', function() {
          that.$el.find('.alert-error').remove();
          that.model.save({
            access_token: 'temp',
            expires_at: new Date().getTime() + 300000,
            expires_in: 300000,
            username: username,
            password: password,
            client_secret: clientSecret,
            client_key: clientKey,
            grant_type: 'password',
            scope: 'PRODUCTION'
          }, {
          	develMode: false,
          	basicAuth: true,
          	data: {
          	  username: username,
              password: password,
              client_secret: clientSecret,
              client_key: clientKey,
              grant_type: 'password',
              scope: 'PRODUCTION'
            },
            username: username,
            clientSecret: clientSecret,
            clientKey: clientKey,
            success: function() {
              message.set('body', message.get('body') + '<p>Success!</p>');
              that.model.store();
              modal.close();
              App.router.navigate('profile', {
                trigger: true
              });
            },
            error: function() {
              console.log("Error");
            	that.$el.prepend($('<div class="alert alert-error">').text('Authentication failed.  Please check your username and password.').fadeIn());
              $('#password').val('');
              modal.close();
            }
          });
        });
        modal.$el.on('hidden', function() {
          modal.remove();
        });
        this.setView('.login-modal', modal);
        modal.render();
      } else {
        this.$el.find('.alert-error').remove().end().prepend($('<div class="alert alert-error">').text('Username and Password are required.').fadeIn());
      }
      return false;
    }
  });

  AgaveAuth.TokenView = Backbone.View.extend({
    template: 'auth/token',
    className: function() {
      var clazz = 'token';
      if (this.model.id === App.Agave.token().id) {
        clazz += ' token-current';
      }
      return clazz;
    },
    initialize: function() {
      this.model.on('change', this.render, this);
    },
    serialize: function() {
      var json = this.model.toJSON();
      json.created_date = moment.unix(json.created).format('YYYY-MM-DDTHH:mm:ssZ');
      json.expires_at = moment.unix(json.expires).format('YYYY-MM-DDTHH:mm:ssZ');
      json.canDelete = this.model.id !== App.Agave.token().id;
      return json;
    },
    events: {
      'click .btn-renew': 'renewToken',
      'click .btn-validate': 'validateToken',
      'click .btn-delete': 'deleteToken'
    },
    renewToken: function() {
      var modalWrap = new UtilViews.ModalView({
        model: new App.Models.MessageModel({
          header: 'Renew Token'
        })
      }),
        renewView = new AgaveAuth.RenewTokenForm({
          model: this.model
        });
      renewView.cleanup = function() {
        modalWrap.close();
      };
      modalWrap.setView('.child-view', renewView);
      modalWrap.$el.on('hidden', function() {
        modalWrap.remove();
      });
      modalWrap.render();
      return false;
    },
    validateToken: function(e) {
      var btn = $(e.currentTarget);
      this.model.fetch({
        silent: true,
        success: function() {
          btn.popover({
            content: 'This token is <span class="label label-success">Valid</span>.',
            html: true,
            placement: 'top',
            trigger: 'manual'
          }).popover('show');
          setTimeout(function() {
            btn.popover('destroy');
          }, 2000);
        },
        error: function() {
          alert('Ohnoes!');
        }
      });
    },
    deleteToken: function() {
      this.model.destroy();
      this.remove();
    }
  });

  AgaveAuth.RenewTokenForm = Backbone.View.extend({
    template: 'auth/renew-token-form',
    events: {
      'submit .renew-form': 'renewToken',
      'click .btn-cancel': 'dismiss'
    },
    renewToken: function() {
      var password = this.$el.find('#password').val(),
      	client_secret = this.$el.find('#client_secret').val(),
        client_key = this.$el.find('#client_key').val();
        that = this;
      this.$el.find('alert-error').remove();
      that.model.save({
      	client_secret: client_secret,
        client_key: client_key,
        }, {
        success: function() {
          that.model.set({
            expires_at: moment().add('hours', 1).unix()
          });
          that.remove();
        },
        error: function() {
          that.$el.prepend('<div class="alert alert-error">Unable to renew token.  Please check your password and try again.</div>');
          that.$el.find('#client_key').val('');
          that.$el.find('#client_secret').val('');
        }
      });
      return false;
    },
    dismiss: function() {
      this.remove();
      return false;
    }
  });

  AgaveAuth.ActiveTokens = Backbone.View.extend({
    template: 'auth/active-tokens',
    initialize: function() {
      this.collection.on('add', function(token) {
        if (token.id === App.Agave.token().id) {
          token = App.Agave.token();
        }
        var view = new AgaveAuth.TokenView({model: token});
        this.insertView('.tokens', view);
        view.render();
      }, this);
      if (App.Agave.token().isValid()) {
        this.collection.add(App.Agave.token());
        this.collection.fetch();
      }
    },
    afterRender: function() {
      if (this.collection.length === 0) {
        this.$el.find('.tokens').html(
        $('<p class="alert alert-warning">').html('<i class="icon-warning-sign"></i> You have no active tokens.'));
      }
    },
    events: {
      'click .btn-new-token': 'getNewToken'
    },
    getNewToken: function() {
      if (App.Agave.token().isValid()) {
        App.router.navigate('auth/renew', {
          trigger: true
        });
      } else {
        App.router.navigate('auth/login', {
          trigger: true
        });
      }
    }
  });

  App.Views.AgaveAuth = AgaveAuth;
  return AgaveAuth;
});
