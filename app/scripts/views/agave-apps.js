/*global define, Backbone, $, _, alert, moment*/
'use strict';
define(['app'], function(App){
  var AgaveApps = {};

  AgaveApps.AppView = Backbone.View.extend({
    template: 'apps/view',
    initialize: function() {
      this.model.on('change', this.render, this);
      if (! this.model.collection) {
        this.model.fetch();
      }
    },
    serialize: function() {
      var json = this.model.toJSON();
      if (this.model.collection) {
        json.hasCollection = true;
      }
      return json;
    },
    events: {
      'click .back-to-list': 'backToCollection',
      'click .btn-show-form': 'showForm',
      'click .btn-show-permissions': 'showPermissions'
    },
    backToCollection: function(e) {
      e.preventDefault();
      this.remove();
      return false;
    },
    showForm: function(e) {
      e.preventDefault();
      if (App.Agave.token().isValid()) {
        new AgaveApps.AppForm({
          model: this.model,
          el: this.$el.find('.app-form')
        }).render();
        $(e.target).hide();
        this.$el.find('.btn-show-permissions').show();
      } else {
        alert('You must be logged in to use applications.');
      }
    },
    showPermissions: function(e) {
      e.preventDefault();
      if (App.Agave.token().isValid()) {
        new AgaveApps.AppsPermissions({
          collection: new Backbone.Agave.Apps.AppPermissionList({ id: this.model.id }),
          el: this.$el.find('.app-form')
        }).render();
        $(e.target).hide();
        this.$el.find('.btn-show-form').show();
      } else {
        alert('You must be logged in to use applications.');
      }
    }
  });

  AgaveApps.AppForm = Backbone.View.extend({
    template: 'apps/form',
    initialize: function() {
    	//this.model.fetch();
    },
    events: {
      'submit form': 'submitForm'
    },
    beforeRender: function() {
	  //this.model.fetch();
      var sorter = function(o) { return o.id; },
        params = _.sortBy(this.model.get('parameters'), sorter),
        inputs = _.sortBy(this.model.get('inputs'), sorter),
        // outputs = _.sortBy(this.model.get('outputs'), sorter),
        fieldset, i;

	  fieldset = new App.Views.FormViews.Fieldset({
        className: 'metadata',
        model: new App.Models.FormModel({legend: 'Job Info'})
      });
      fieldset.insertView(new App.Views.FormViews.Text({
        model: new App.Models.FormModel({
          id: 'jobName',
          name: 'jobName',
          label: 'Job name',
          required: true,
          defaultValue: this.model.get('name') + '-' + moment().unix(),
          help: 'Provide a name for this job'
        })
      }));
      fieldset.insertView(new App.Views.FormViews.Text({
        model: new App.Models.FormModel({
          id: 'requestedTime',
          name: 'requestedTime',
          label: 'Requested wall time',
          required: this.model.get('defaultMaxRunTime') == null ? true : false,
          defaultValue: this.model.get('defaultMaxRunTime') != null ? this.model.get('defaultMaxRunTime') : '00:01:00',
          help: 'Enter your requested Wall Time (hh:mm:ss)'
        })
      }));
      fieldset.insertView(new App.Views.FormViews.Text({
        model: new App.Models.FormModel({
          id: 'nodeCount',
          name: 'nodeCount',
          label: 'Node count',
          required: false,
          defaultValue: this.model.get('defaultNodeCount') != null ? this.model.get('defaultNodeCount') : '1',
          help: 'Enter the number of nodes needed to run this job'
        })
      }));

      fieldset.insertView(new App.Views.FormViews.Text({
        model: new App.Models.FormModel({
          id: 'processorsPerNode',
          name: 'processorsPerNode',
          label: 'Processors per node',
          required: false,
          defaultValue: this.model.get('defaultProcessorsPerNode') != null ? this.model.get('defaultProcessorsPerNode') : '1',
          help: 'Enter the number of processors per node to use when running this job'
        })
      }));

      fieldset.insertView(new App.Views.FormViews.Text({
        model: new App.Models.FormModel({
          id: 'memoryPerNode',
          name: 'memoryPerNode',
          label: 'Memory per node',
          required: this.model.get('defaultMemoryPerNode') == null ? true : false,
          defaultValue: this.model.get('defaultMemoryPerNode') != null ? this.model.get('defaultMemoryPerNode') : '2',
          help: 'Enter the amount of memory per node in GB to use when running this job'
        })
      }));

      this.insertView('.form-fields', fieldset);

      if (params && params.length > 0) {
        fieldset = new App.Views.FormViews.Fieldset({
          className: 'parameters',
          model: new App.Models.FormModel({
            legend: 'Parameters'
          })
        });

        for (i = 0; i < params.length; i++) {
          var param = params[i];
          if (param.value.visible) {
            var model = new App.Models.FormModel({
              id: param.id,
              name: param.id,
              label: param.details.label,
              help: param.details.description === '' ? null : param.details.description,
              defaultValue: param.defaultValue,
              required: param.value.required
            });

            switch (param.value.type) {
            case 'bool':
              model.set('defaultValue', param.defaultValue === 'true');
              fieldset.insertView(new App.Views.FormViews.Checkbox({model: model}));
              break;

            case 'string':
              var regexp = /^([^|]+[|])+[^|]+$/;
              if (regexp.test(param.value.validator)) {
                model.set('options', param.value.validator.split('|'));
                fieldset.insertView(new App.Views.FormViews.Select({model: model}));
              } else {
                fieldset.insertView(new App.Views.FormViews.Text({model: model}));
              }
              break;

            default:
              fieldset.insertView(new App.Views.FormViews.Text({model: model}));
            }
          }
        }
        this.insertView('.form-fields', fieldset);
      }

      if (inputs && inputs.length > 0) {
        fieldset = new App.Views.FormViews.Fieldset({
          className: 'inputs',
          model: new App.Models.FormModel({
            legend: 'Input files'
          })
        });

        for (i = 0; i < inputs.length; i++) {
          fieldset.insertView(new App.Views.AgaveIO.FileChooser({
            model: new App.Models.FormModel({
              id: inputs[i].id,
              name: inputs[i].id,
              label: inputs[i].details.label,
              help: inputs[i].details.description === '' ? null : inputs[i].details.description,
              defaultValue: inputs[i].defaultValue,
              required: inputs[i].value.required
            })
          }));
        }
        this.insertView('.form-fields', fieldset);
      }

      // if (outputs && outputs.length > 0) {
      //   fieldset = new App.Views.FormViews.Fieldset({
      //     className: 'outputs',
      //     model: new App.Models.FormModel({
      //       legend: 'Output files'
      //     })
      //   });

      //   for (i = 0; i < outputs.length; i++) {
      //     fieldset.insertView(new App.Views.FormViews.File({
      //       model: new App.Models.FormModel({
      //         id: outputs[i].id,
      //         name: outputs[i].id,
      //         label: outputs[i].details.label,
      //         help: outputs[i].details.description === '' ? null : outputs[i].details.description,
      //         defaultValue: outputs[i].defaultValue,
      //         required: outputs[i].value.required
      //       })
      //     }));
      //   }
      //   this.insertView('.form-fields', fieldset);
      // }


    },
    submitForm: function(e) {
      e.preventDefault();
      var form = $(e.currentTarget).closest('form');
      var params = $('.parameters', form).serializeArray(),
        inputs = $('.inputs', form).serializeArray(),
        // outputs = $('.outputs', form).serializeArray(),
        job = {},
        i;
      if (params.length > 0) {
        for (i = 0; i < params.length; i++) {
          if (params[i].value !== '') {
            job[params[i].name] = params[i].value;
          }
        }
      }
      if (inputs.length > 0) {
        for (i = 0; i < inputs.length; i++) {
          if (inputs[i].value !== '') {
            job[inputs[i].name] = inputs[i].value;
          }
        }
      }
      // if (outputs.length > 0) {
      //   for (i = 0; i < outputs.length; i++) {
      //     if (outputs[i].value !== '') {
      //       job[outputs[i].name] = outputs[i].value;
      //     }
      //   }
      // }
      job.name = form[0].jobName.value;
      job.appId = this.model.id;
      job.requestedTime = form[0].requestedTime.value;
      // todo?
      job.archive = true;
      job.archivePath = '/' + App.Agave.token().get('username') + '/' + job.jobName;
      var appJob = new Backbone.Agave.Jobs.Job();
      appJob.save(job, {
        success: function() {
          alert('success');
        }
      });
      return false;
    }
  });

  AgaveApps.AppsList = Backbone.View.extend({
    template: 'apps/list',
    initialize: function() {
      this.collection.on('reset', this.render, this);
      this.collection.fetch({reset:true});
    },
    serialize: function() {
      return {apps: this.collection.toJSON()};
    },
    events: {
      'click .view-app': 'viewApplication'
    },
    viewApplication: function(e) {
      e.preventDefault();
      var models = this.collection.where({id: e.target.dataset.id});
      if (models) {
      	var appModel = new Backbone.Agave.Apps.Application({id: models[0].id});
        var view = new AgaveApps.AppView({model: appModel}); //models[0]});
        App.Layouts.main.setView('.content', view);
        view.render();
        App.router.navigate('#apps/' + models[0].id);
        $('html,body').animate({scrollTop:view.$el.position().top - 100});
      }
      return false;
    }
  });

  AgaveApps.AppsPermissions = Backbone.View.extend({
    template: 'apps/permissions',
    initialize: function() {
      this.collection.on('reset', this.render, this);
      this.collection.fetch({reset:true});
    },
    serialize: function() {
      return {permissions: this.collection.toJSON(), appId: this.collection.appId};
    },
    events: {
      'click .edit-app-pem': 'editAppPermission',
      'click .add-app-pem': 'addAppPermission'
    },
    editAppPermission: function(e) {
      e.preventDefault();
      var models = this.collection.where({id: e.target.dataset.id});
      if (models) {
      	models[0].appId = this.collection.appId;
      	//var appPemModel = new Backbone.Agave.Apps.AppPermissionForm({appId: , model: models[0]});
        var view = new AgaveApps.AppPermissionForm({model: models[0]});
        App.Layouts.main.setView('.content', view);
        view.render();
        App.router.navigate('#apps/' + models[0].id + '/pems/edit/' + appPemModel.get('username'));
        $('html,body').animate({scrollTop:view.$el.position().top - 100});
      }
      return false;
    },
    addAppPermission: function(e) {
      e.preventDefault();
      var appModel = new Backbone.Agave.Apps.AppPermission({appId: this.collection.appId});
      var view = new AgaveApps.AppPermissionForm({model: appModel});
      App.Layouts.main.setView('.content', view);
      view.render();
      App.router.navigate('#apps/' + this.collection.appId + '/pems/edit');
      $('html,body').animate({scrollTop:view.$el.position().top - 100});

      return false;
    }
  });

  AgaveApps.AppPermissionForm = Backbone.View.extend({
    tagName:'form',
    template: 'apps/permisison-form',
    initialize: function() {
       this.model.fetch();
    },
    beforeRender: function() {
      var FormViews = App.Views.FormViews;
	  //this.model.fetch();
      var permissionName = this.model.getPermissionName();

      this.insertViews({'.form-fields': [
        new FormViews.Text({
          model: new App.Models.FormModel({
            label: 'Username',
            help: 'This is the user for whom the permission applies',
            name: 'username',
            id: 'username',
            defaultValue: this.model.get('username'),
            required: true
          })
        }),
        new FormViews.Hidden({
          model: new App.Models.FormModel({
            name: 'appId',
            id: 'appId',
            defaultValue: this.model.get('appId')
          })
        }),
        new FormViews.Select({
          model: new App.Models.FormModel({
            label: 'Permission',
            help: 'This is the HTTP Method with which this URL will be invoked',
            name: 'permission',
            id: 'permission',
            defaultValue: permissionName,
            options: ['READ','WRITE','EXECUTE','READ_WRITE','READ_EXECUTE','WRITE_EXECUTE', 'ALL', 'NONE'],
            required: true
          })
        }),
      ]});

      this.insertViews({'.form-actions': [
        new FormViews.Button({
          'name': 'pem-create-submit',
          'id': 'pem-create-submit',
          'type': 'submit',
          'class': 'btn btn-success',
          'value': 'Update Permission'
        }),
        new FormViews.Button({
          'name': 'pem-create-cancel',
          'id': 'pem-create-cancel',
          'value': 'Cancel'
        })
      ]});
    },
    events: {
      'click #pem-create-submit': 'submitForm',
      'click #pem-create-cancel': 'cancelForm'
    },
    submitForm: function(e) {
      e.preventDefault();
      var formValues = this.$el.serializeArray();
      var post = {};

      _.each(formValues, function(value) {
        post[value.name] = value.value;
      });
      var appPem = new Backbone.Agave.Apps.AppPermission({appId:post['appId']});
      this.model.save({}, {
        emulateJSON: true,
        data: post,
        success: function() {
          Backbone.history.navigate('#apps/' + post.appId, {'trigger':true});
        }
      });
      return false;
    },
    cancelForm: function(e) {
      e.preventDefault();
      var fragment;
      if (App.Agave.token().isValid()) {
        fragment = '#apps/' + this.model.get('appId');
      } else {
        fragment = '#';
      }
      Backbone.history.navigate(fragment, {'trigger':true});
      return false;
    }
  });

  App.Views.AgaveApps = AgaveApps;
  return AgaveApps;
});
