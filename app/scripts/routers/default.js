/*global define, Backbone*/
'use strict';
define(['app'], function(App) {
  var DefaultRouter = Backbone.Router.extend({
    routes: {
      '': 'index',
      'auth': 'authIndex',
      'auth/login': 'authLogin',
      'auth/new': 'authNew',
      'auth/check': 'authCheck',
      'auth/renew': 'authRenew',
      'auth/logout': 'authLogout',

	    'apps(/)': 'appsList',
//       'apps/search': 'appsSearch',
      'apps/:id/pems(/)': 'appsPermissions',
      'apps/:id/pems/edit(/:username)': 'appsPermissionsEdit',
      'apps/:id(/)': 'appsView',
      'apps/system/:id(/)': 'appsSystemList',
//
      'jobs(/)': 'jobsList',
//       'jobs/search': 'jobsSearch',
//       'jobs/:id/pems': 'jobsPermissions',
//       'jobs/:id/outputs': 'jobsOutputs',
      'jobs/:id/history': 'jobHistory',
      'jobs/:id': 'jobView',
//
      'systems(/)': 'systemsList',
//       'systems/search': 'systemsSearch',
//       'systems/:id/pems': 'systemsPermissions',
//       'systems/:id/credentials': 'systemsCredentials',
      'systems/:id(/)': 'systemView',
//
      'files/browse/:systemId(/*path)': 'fileSystemBrowser',
      'files/pems/:systemId(/*path)': 'filePermissions',
      'files/history/:systemId(/*path)': 'fileHistory',
//
//       'transforms/': 'transformsList',
//       'transforms/search/': 'transformsList',
//       'transforms/:id': 'transformsView',
//
      'meta/data(/)': 'metadataList',
      'meta/data/edit(/:id)': 'metadataEdit',
      'meta/data/search': 'metadataSearch',
      'meta/data/:id/pems(/)': 'metadataPermissions',
      'meta/data/:id/pems/edit(/:username)': 'metadataPermissionsEdit',
      'meta/data/:id': 'metadataView',
//
//       'schema/': 'schemaList',
//       'schema/search': 'schemaSearch',
//       'schema/:id/pems': 'schemaPermissions',
//       'schema/:id': 'schemaView',
//
      'notifications(/)': 'notificationsList',
      'notifications/search': 'notificationsSearch',
      'notifications/edit': 'notificationsCreate',
      'notifications/:id': 'notificationsView',

      'postits(/)': 'currentPostIts',
      'postits/create': 'postItForm',

      'profile(/)': 'myProfile',
      'profile/search': 'searchProfile'
    },

    index: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AppViews.Home());
      App.Layouts.main.render();
    },

    authLogin: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveAuth.NewTokenForm({model: App.Agave.token()}));
      App.Layouts.main.render();
    },

    authNew: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveAuth.NewTokenForm({model: new Backbone.Agave.Auth.Token()}));
      App.Layouts.main.render();
    },

    authRenew: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveAuth.RenewTokenForm({model: new Backbone.Agave.Auth.Token()}));
      App.Layouts.main.render();
    },

    authLogout: function() {
      App.Agave.token().clear();
      App.router.navigate('', {trigger:true});
    },

    systemsList: function() {
      App.Layouts.main.template = 'two-col';
      App.Layouts.main.setView('.columns-header', new App.Views.Util.Message({
        model: new App.Models.MessageModel({body:'Your Systems'}),
        tagName: 'h1'
      }));
      App.Layouts.main.setView('.sidebar', new App.Views.AgaveSystem.List({collection: new Backbone.Agave.Systems.Listing()}));
      App.Layouts.main.setView('.content', new App.Views.Util.Alert({model: new App.Models.MessageModel({
        header: 'Choose an app',
        body: 'Select a system to view its details.'
      })}));
      App.Layouts.main.render();
    },

    systemView: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveSystem.SystemView({model: new Backbone.Agave.Systems.System({id:id})}));
      App.Layouts.main.render();
    },

    jobsList: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveJobs.List());
      App.Layouts.main.render();
    },

    jobHistory: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveJobs.History({ model: new Backbone.Agave.Jobs.History({id: id})}));
      App.Layouts.main.render();
    },

    jobView: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveJobs.JobView({model: new Backbone.Agave.Jobs.Job({id:id})}));
      App.Layouts.main.render();
    },

	  appsList: function() {
      App.Layouts.main.template = 'two-col';
      App.Layouts.main.setView('.columns-header', new App.Views.Util.Message({
        model: new App.Models.MessageModel({body:'Available Apps'}),
        tagName: 'h1'
      }));
      App.Layouts.main.setView('.sidebar', new App.Views.AgaveApps.AppsList({collection: new Backbone.Agave.Apps.Applications()}));
      App.Layouts.main.setView('.content', new App.Views.Util.Alert({model: new App.Models.MessageModel({
        header: 'Choose an app',
        body: 'Select an app to view its details.'
      })}));
      App.Layouts.main.render();
    },

    appsSystemList: function(id) {
      App.Layouts.main.template = 'two-col';
      App.Layouts.main.setView('.columns-header', new App.Views.Util.Message({
        model: new App.Models.MessageModel({body:'Available Apps'}),
        tagName: 'h1'
      }));
      App.Layouts.main.setView('.sidebar', new App.Views.AgaveApps.AppsList({collection: new Backbone.Agave.Apps.SystemApplications({id: id})}));
      App.Layouts.main.setView('.content', new App.Views.Util.Alert({model: new App.Models.MessageModel({
        header: 'Choose an app',
        body: 'Select an app to view its details.'
      })}));
      App.Layouts.main.render();
    },

    appsView: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveApps.AppView({model: new Backbone.Agave.Apps.Application({id:id})}));
      App.Layouts.main.render();
    },

    appsPermissions: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveApps.AppsPermissions({collection: new Backbone.Agave.Apps.AppPermissionList({id:id})}));
      App.Layouts.main.render();
    },

    appsPermissionsEdit: function(id, username) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveApps.AppPermissionForm({model: new Backbone.Agave.Apps.AppPermission({appId:id, username:username})}));
      App.Layouts.main.render();
    },

    metadataList: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveMetadata.MetadataList({collection: new Backbone.Agave.Metadata.MetadataList()}) );
      App.Layouts.main.render();
    },

    metadataListByAssociatedId: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveMetadata.MetadataList({collection: new Backbone.Agave.Metadata.Search({associatedUuid: id})}) );
      App.Layouts.main.render();
    },

    metadataView: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveMetadata.MetadataView({model: new Backbone.Agave.Metadata.Metadata({id:id})}));
      App.Layouts.main.render();
    },

    metadataEdit: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveMetadata.CreateForm({model: new Backbone.Agave.Metadata.Metadata({id:id})}));
      App.Layouts.main.render();
    },

    metadataPermissions: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveMetadata.MetadataPermissions({collection: new Backbone.Agave.Metadata.MetadataPermissionList({id:id})}));
      App.Layouts.main.render();
    },

    metadataPermissionsEdit: function(id, username) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveMetadata.MetadataPermissionForm({model: new Backbone.Agave.Metadata.MetadataPermission({metaId:id, username:username})}));
      App.Layouts.main.render();
    },

    fileSystemBrowser: function(systemId, path) {
      if (systemId === null || systemId == 'default') {
      	systemId = 'data.iplantcollaborative.org';
        if (path == null) path = App.Agave.token().get('username');
        App.router.navigate('files/browse/'+systemId+'/'+path, {
          trigger: true
        });
      }
      else if (systemId == 'data.iplantcollaborative.org') {
        if (path == null) {
          path = App.Agave.token().get('username');
          App.router.navigate('files/browse/'+systemId+'/'+path, {
            trigger: true
          });
        }
      }
      else {
      	if (path == null) path = '';
      }

      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content',new App.Views.AgaveIO.Browser({collection: new Backbone.Agave.IO.Listing([], {path: path, system: systemId})}));
      App.Layouts.main.render();
    },

    filePermissions: function(systemId, path) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content',new App.Views.AgaveIO.Browser({collection: new Backbone.Agave.IO.FilePermissions([], {path: path, system: systemId})}));
      App.Layouts.main.render();
    },

    fileHistory: function(systemId, path) {
      if (systemId === null || systemId == 'default')
      	systemId = 'data.iplantcollaborative.org';
      	if (path == null) path = App.Agave.token().get('username');
      else
      	if (path == null) path = '';
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content',new App.Views.AgaveIO.History({collection: new Backbone.Agave.IO.HistoryListing([], {path: path, system: systemId})}));
      App.Layouts.main.render();
    },

    notificationsList: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveNotification.List());
      App.Layouts.main.render();
    },

    notificationsCreate: function(notificationId) {
      App.Layouts.main.template = 'one-col';
      if (notificationId) {
        App.Layouts.main.setView('.content', new App.Views.AgaveNotification.CreateForm({model: new Backbone.Agave.Notification.Notification({id:notificationId})}));
      } else {
        App.Layouts.main.setView('.content', new App.Views.AgaveNotification.CreateForm({model: new Backbone.Agave.Notification.Notification()}));
      }
      App.Layouts.main.render();
    },

    notificationsSearch: function(notificationId) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveNotification.Search({ collection: new Backbone.Agave.Notification.Search() }));
      App.Layouts.main.render();
    },

    notificationsView: function(notificationId) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveNotification.ListItem({model: new Backbone.Agave.Notification.Notification({id:notificationId})}));
      App.Layouts.main.render();
    },

    currentPostIts: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgavePostIt.List());
      App.Layouts.main.render();
    },

    postItForm: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgavePostIt.CreateForm());
      App.Layouts.main.render();
    },

    myProfile: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveProfile.MyProfile());
      App.Layouts.main.render();
    },

    searchProfile: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveProfile.Search({
        collection: new Backbone.Agave.Profile.Search()
      }));
      App.Layouts.main.render();
    }
  });

  App.Routers.DefaultRouter = DefaultRouter;
  return DefaultRouter;
});
