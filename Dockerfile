#
# Agave ToGo Dockerfile
#
# Repository tag: agaveapi/agave-togo
#
# https://bitbucket.org/deardooley/agave-gateway-dna
#

# Pull base image.
FROM dockerfile/nodejs

RUN npm install -g yo grunt-cli bower

RUN apt-get install -y ruby

RUN gem install saas compass

RUN \
  cd /tmp && \
  git clone https://bitbucket.org/deardooley/agave-gateway-dna.git

RUN \
  cd /tmp/agave-gateway-dna && \
  npm install

RUN \
  cd /tmp/agave-gateway-dna && \
  bower install --allow-root -f

RUN \
  cd /tmp/agave-gateway-dna && \
  grunt build

EXPOSE 9000

WORKDIR /tmp/agave-gateway-dna

# Define default command.
ENTRYPOINT grunt server
