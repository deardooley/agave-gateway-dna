Agave ToGo
===================

This is a reference implementation of the iPlant Agave ToGo reference gateway using Backbone.js and Mustache.


## Running with Docker

The easiest way to run this application is to run the existing Agave ToGo Docker container. If you have not heard of [Docker](http://docker.com), you should check it out. It makes packaging and shipping software as easy as build once, run anywhere. The instructions below show you how easy it is to run this app as a container.


### Requirements

You will need [Docker](http://docker.com) installed and running somewhere. This can be in a vm or on your local system. More information on installing Docker can be found [here](http://docs.docker.com/installation/).


### Project setup

This app is available as a prebuilt Docker container from the Docker central repository. The following commands will run the container and map the compass server running on port 9000 inside the container to port 9000 on your machine so you can access the app by browsing to `http://localhost:9000`.

    docker pull agaveapi/agave-togo
    docker run -t -i -p 9000:9000 agaveapi/agave-togo

If you would prefer to build the container from scratch, you can use the included Dockerfile to do so.

    docker build --tag="agave-togo" .
    docker run -t -i -p 9000:9000 agave-togo

## Installing locally

You can also build the application locally and optionally package it up as static html, css, and javascript files for hosting on your local web server, or out of your Dropbox, campus web server, Google Drive, etc.

### Requirements

The following are not requirements to run the application, but are used in the build process
and for other tasks during development

- node.js + npm (v0.8.18+)
- yeoman
- grunt-cli (0.1.7+)
- bower (0.8.6+)
- ruby + rubygems (required for sass + compass)
- sass (3.2.7+)
- compass (0.12.2)

#### Node.js

Node and NPM are used for installing packages necessary for the rest of the build process.
Install node.js and npm via whatever install process is available.

http://nodejs.org/download/

#### Yeoman, Grunt, Bower

After you have installed node and npm, you can install yeoman, grunt, and bower.

    npm install -g yo grunt-cli bower

#### Sass + Compass

Make sure you have ruby and rubygems installed on your system.

    gem install sass compass


### Project setup

Check out the project and cd into the project.  Use npm and bower to install the rest of the project dependencies.

    git clone https://bitbucket.org:deardooley/agave-gateway-dna.git
    cd agave-gateway-dna
    npm install
    bower install

#### Start the application in node development server

This will start a node server on localhost:9000 and automatically open the default browser to the application. Using this for development

    grunt server

#### Building releases

There are two release modes available, production and human-friendly.  There are builds of the application that can be dropped into a webserver, a content delivery network (CDN), or even served straight out of Dropbox     or Google drive!

You can build a production release (catted, minified, and uglified) with

    grunt build

This will build the application in {{project_home}}/dist.


You can build a human-friendly release (expanded and readable) with

    grunt debug

This will build the application in {{project_home}}/debug.  Note, this build is **not production-friendly**.
Resources such as templates and module dependencies are fetched synchronously, so the application will occasionally hang for a moment while the assets download.
